**Instalação**

**1. Instalação do kubectl:"

`sudo apt-get install curl -y`

`curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"`

`chmod +x ./kubectl`

**2. Instalação do minikube:"

`curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \ && chmod +x minikube`

`sudo mkdir -p /usr/local/bin/`

`sudo install minikube /usr/local/bin/`

**3. Iniciar cluster de uma VM**

`minikube start --vm-driver=virtualbox` - cria/inicializa um cluster dentro de uma vm

_**A cada inicialização do Linux é preciso novamente rodar o comando acima**_

`kubectl get node` - listar os clusters criados pelo minikube

`kubectl run nginx-pod --image=nginx:latest` - criar pod com imagem nginx(última versão)

`kubectl get pods` - listar pods criados

`kubectl get pods --watch` - listar pods criados e verificar em tempo real mudança de estado

`kubectl describe pod nginx-pod` - listar diversas informações

`kubectl edit pod nginx-pod` - editar informações de um pod

_Editor padrão do Ubuntu é o Vi, para mudar siga os passos abaixo:_

- Editar arquivo ~/.bashrc

- Na última linha adicionar o código abaixo:

`KUBE_EDITOR=/usr/bin/gedit`

`export KUBE_EDITOR`

- Salvar e carregue novamento o bashrc, pelo comando:

`source ~/.bashrc`

`kubectl apply -f primeiro-pod.yml` - criar pods por meio de arquivos declarativos

Site para validar o YAML para Kubernetes
https://kubeyaml.com/

`kubectl delete pod nginx-pod` - deletar pod pelo nome

`kubectl delete -f primeiro-pod.yml` - deletar por arquivo de definição

`kubectl exec -it portal-noticias -- bash` - executar comando dentro do container de maneira interativa depois do -- será o comando enviado

`kubectl get pods -o wide` - verificar qual IP do pod

**1. Serviços**

`kubectl apply -f svc-pod-2.yaml` - criar serviço

`kubectl get svc ou kubectl get service` - listar todos serviços criados

_**nodePort vai de 30000-32767**_

`kubectl get nodes -o wide` - verificar IP externo do nó

`kubectl logs db-noticias` - logs do pod

`kubectl get configmap` - lista Config Maps criados

`kubectl describe confimap NOME` - detalha o confimap do NOME

_Mais informações sobre NodePort e IP's_:
https://kubernetes.io/docs/concepts/services-networking/service/#nodeport

`kubectl get replicasets` - listar replicasets criados

`kubectl get deployments` - listar deployments criados

`kubectl rollout history deployment NOME_DEPLOY` - veremos toda a história do deployment

`kubectl apply -f nginx-deployment.yaml --record` - habilita logs de deployment

`kubectl annotate deployment nginx-deployment kubernetes.io/change-cause="Definindo a imagem com versão latest"` - cria log na versão de deployment

`kubectl rollout undo deployment nginx-deployment --to-revision=2` - rollback para versão desejada do deployment

`kubectl exec -it NOME-POD --container NOME-CONTAINER -- bash` - rodar terminal de um container dentro de um Pod.

`minikube ssh` - acessar máquina virtual minikube

`minikube addons enable metrics-server` - habilitar metric-server no Minikube

``

``

